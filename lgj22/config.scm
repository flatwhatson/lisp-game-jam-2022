(define-module (lgj22 config)
  #:export (data-file))

(define %data-dir
  (delay
    (canonicalize-path
     (or (getenv "GAMEJAM_DATADIR")
         (and=> (current-filename)
                (lambda (filename)
                  (string-append (dirname (current-filename)) "/../data")))
         (error "Please set the GAMEJAM_DATADIR variable")))))

(define (data-file filename)
  (string-append (force %data-dir) "/" filename))
