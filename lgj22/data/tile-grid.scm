(define-module (lgj22 data tile-grid)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (ice-9 match)
  #:export (make-tile-grid
            list->tile-grid
            strings->tile-grid
            tile-grid-tabulate
            tile-grid-ref
            tile-grid-set!
            tile-grid-fold
            tile-grid-fold-3x3
            tile-grid-fold-neighbours))

(define-record-type <tile-grid>
  (%make-tile-grid cols rows cells default)
  grid?
  (cols tile-grid-cols)
  (rows tile-grid-rows)
  (cells tile-grid-cells)
  (default tile-grid-default))

(define (display-tile-grid g port)
  (format port "#<tile-grid ~a x ~a>"
          (tile-grid-cols g)
          (tile-grid-rows g)))

(set-record-type-printer! <tile-grid> display-tile-grid)

(define* (make-tile-grid cols rows #:optional default)
  "Create a grid with dimensions COLS x ROWS.

Grid cells will be initialized to #f, or DEFAULT if provided.  The
DEFAULT will also be used when a cell has no value."
  (let ((cells (make-vector (* cols rows) default)))
    (%make-tile-grid cols rows cells default)))

(define* (list->tile-grid rows #:optional default)
  "Create a grid from ROWS; a list of lists of cell values.

Each inner list represents the values in a single row.  The number of
rows and number of values in the first row determine the dimensions of
the grid.  An optional DEFAULT value can be specified, for use when a
cell has no value.

Note that (0,0) is the bottom-left corner of the grid."
  (let* ((rows (reverse rows))
         (ncols (length (car rows)))
         (nrows (length rows))
         (cells (make-vector (* ncols nrows))))
    (let loop ((row (car rows)) (rows (cdr rows)) (ix 0))
      (cond ((and (null? row) (null? rows))
             (%make-tile-grid ncols nrows cells default))
            ((null? row)
             (loop (car rows) (cdr rows) ix))
            (else
             (vector-set! cells ix (car row))
             (loop (cdr row) rows (1+ ix)))))))

(define* (strings->tile-grid strings #:optional default)
  "Create a grid from STRINGS; a list of strings where each character is a
single cell value.

Each string represents the values in a single row.  The number of rows
and number of characters in the first string determine the dimensions of
the grid.  An optional DEFAULT value can be specified, for use when a
cell has no value.

Note that (0,0) is the bottom-left corner of the grid."
  (list->tile-grid (map string->list strings) default))

(define* (tile-grid-tabulate cols rows proc #:optional default)
  "Apply PROC to generate a grid with dimensions COLS x ROWS.

Each PROC call is '(PROC X Y)', where X and Y are the cell coordinates.
The return value of PROC is the value of that cell in the grid."
  (let ((cells (make-vector (* cols rows))))
    (let loop ((x 0) (y 0) (ix 0))
      (cond ((>= y rows)
             (%make-tile-grid cols rows cells default))
            ((>= x cols)
             (loop 0 (1+ y) ix))
            (else
             (vector-set! cells ix (proc x y))
             (loop (1+ x) y (1+ ix)))))))

(define (tile-grid-index g x y)
  (let ((cols (tile-grid-cols g))
        (rows (tile-grid-rows g)))
    (and (>= x 0) (< x cols)
         (>= y 0) (< y rows)
         (+ x (* y cols)))))

(define (tile-grid-ref grid x y)
  "Return the value at the X,Y coordinate in GRID."
  (vector-ref (tile-grid-cells grid) (tile-grid-index grid x y)))

(define (tile-grid-set! grid x y val)
  "Store VAL at the X,Y coordinate in GRID."
  (vector-set! (tile-grid-cells grid) (tile-grid-index grid x y) val))

(define (tile-grid-fold proc init grid)
  "Apply PROC to each cell in GRID to build a result.

Each PROC call is '(PROC X Y ELEM PREVIOUS)', where X and Y are the cell
coordinates, and ELEM is the cell value.  PREVIOUS is the return from
the last call to PROC, or INIT for the first call."
  (let ((cols (tile-grid-cols grid))
        (rows (tile-grid-rows grid))
        (cells (tile-grid-cells grid)))
    (let loop ((x 0) (y 0) (ix 0) (res init))
      (cond ((>= y rows)
             res)
            ((>= x cols)
             (loop 0 (1+ y) ix res))
            (else
             (let* ((val (vector-ref cells ix))
                    (next (proc x y val res)))
               (loop (1+ x) y (1+ ix) next)))))))

(define (tile-grid-fold-3x3 f init g)
  "Apply PROC to each non-overlapping 3x3 tile in GRID to build a result.

Each PROC call is '(PROC X Y NW N NE W ELEM E SW S SE PREVIOUS)', where
X and Y are the coordinates of the center cell, ELEM is the center cell
value, and NW/N/NE/W/E/SW/S/SE are neighbouring cell values.  PREVIOUS
is the return from the last call to PROC, or INIT for the first call."
  (define cols (tile-grid-cols g))
  (define rows (tile-grid-rows g))
  (define cells (tile-grid-cells g))

  (define last-col (- cols 1))
  (define 2last-row (- rows 2))
  (define 2rows (* 2 cols))

  (define (fetch ix)
    (vector-ref cells ix))

  (define (index x y)
    (+ x (* y cols)))

  (let loop ((x 1) (y 1)
             (nw (index 0 2)) (nx (index 1 2)) (ne (index 2 2))
             (yw (index 0 1)) (yx (index 1 1)) (ye (index 2 1))
             (sw (index 0 0)) (sx (index 1 0)) (se (index 2 0))
             (res init))
    (cond ((< x last-col)
           (loop (+ x 3) y
                 (+ nw 3) (+ nx 3) (+ ne 3)
                 (+ yw 3) (+ yx 3) (+ ye 3)
                 (+ sw 3) (+ sx 3) (+ se 3)
                 (f x y
                    (fetch nw) (fetch nx) (fetch ne)
                    (fetch yw) (fetch yx) (fetch ye)
                    (fetch sw) (fetch sx) (fetch se) res)))
          ((< y 2last-row)
           (loop 1 (+ y 3)
                 (+ nw 2rows) (+ nx 2rows) (+ ne 2rows)
                 (+ yw 2rows) (+ yx 2rows) (+ ye 2rows)
                 (+ sw 2rows) (+ sx 2rows) (+ se 2rows) res))
          (else
           res))))

(define (tile-grid-fold-neighbours proc init grid)
  "Apply PROC to the 3x3 neighbourhood of each cell in GRID to build a result.

Each PROC call is '(PROC X Y NW N NE W ELEM E SW S SE PREVIOUS)', where
X and Y are the cell coordinates, ELEM is the cell value, and
NW/N/NE/W/E/SW/S/SE are neighbouring cell values.  PREVIOUS is the
return from the last call to PROC, or INIT for the first call."

  (define cols (tile-grid-cols grid))
  (define rows (tile-grid-rows grid))
  (define cells (tile-grid-cells grid))
  (define __ (tile-grid-default grid))

  (define last-col (1- cols))
  (define last-row (1- rows))
  (define 2last-row (1- last-row))

  (define (fetch ix)
    (vector-ref cells ix))

  (define (index x y)
    (+ x (* y cols)))

  ;; Process the bottom row.  There are never southern neighbours, so we
  ;; avoid unnecessary work trying to load them.
  (define first-pass
    (let loop ((x 0)
               (nw __) (nx (fetch (index 0 1))) (ix1 (index 1 1))
               (yw __) (yx (fetch (index 0 0))) (ix0 (index 1 0))
               (res init))
      (cond ((< x last-col)
             ;; This position has eastern neighbours, load them and
             ;; continue east.
             (let ((ne (fetch ix1))
                   (ye (fetch ix0)))
               (loop (1+ x)
                     nx ne (1+ ix1)
                     yx ye (1+ ix0)
                     (proc x last-row
                           nw nx ne
                           yw yx ye
                           __ __ __ res))))
            (else
             ;; This is the south-eastern corner.  Return the result of
             ;; processing the last column of the bottom row.
             (proc x 0
                   nw nx __
                   yw yx __
                   __ __ __ res)))))

  ;; Process from the second to the second-last row.
  (define main-pass
    (let loop ((x 0) (y 1)
               (nw __) (nx (fetch (index 0 2))) (ix2 (index 1 2))
               (yw __) (yx (fetch (index 0 1))) (ix1 (index 1 1))
               (sw __) (sx (fetch (index 0 0))) (ix0 (index 1 0))
               (res first-pass))
      (cond ((and (< x last-col) (< y last-row))
             ;; This position has eastern neighbours, load them and
             ;; continue east.
             (let ((ne (fetch ix2))
                   (ye (fetch ix1))
                   (se (fetch ix0)))
               (loop (1+ x) y
                     nx ne (1+ ix2)
                     yx ye (1+ ix1)
                     sx se (1+ ix0)
                     (proc x y
                           nw nx ne
                           yw yx ye
                           sw sx se res))))
            ((< y 2last-row)
             ;; This is the right edge with no eastern neighbours.  Load
             ;; the first column of the next row and continue there.
             (loop 0 (1+ y)
                   __ (fetch ix0) (1+ ix2)
                   __ (fetch ix1) (1+ ix1)
                   __ (fetch ix2) (1+ ix0)
                   (proc x y
                         nw nx __
                         yw yx __
                         sw sx __ res)))
            (else
             ;; This is the right edge of the second-last row.  Return
             ;; the result of processing everything before the last row.
             (proc x y
                   nw nx __
                   yw yx __
                   sw sx __ res)))))

  ;; Processes the top row.  There are never northern neighbours, so we
  ;; avoid unnecessary work trying to load them.
  (define last-pass
    (let loop ((x 0)
               (yw __) (yx (fetch (index 0 last-row)))  (ix2 (index 1 last-row))
               (sw __) (sx (fetch (index 0 2last-row))) (ix1 (index 1 2last-row))
               (res main-pass))
      (cond ((< x last-col)
             ;; This position has eastern neighbours, load them and
             ;; continue east.
             (let ((ye (fetch ix2))
                   (se (fetch ix1)))
               (loop (1+ x)
                     yx ye (1+ ix2)
                     sx se (1+ ix1)
                     (proc x 0
                           __ __ __
                           yw yx ye
                           sw sx se res))))
            (else
             ;; This is the north-eastern corner.  Return the result of
             ;; processing everything in the first row.
             (proc x last-row
                   __ __ __
                   yw yx __
                   sw sx __ res)))))

  last-pass)

;; Basic implementation of fold-neighbours for testing.
(define (tile-grid-fold-neighbours-naive f init g)
  (define (ref x y)
    (and (>= x 0) (>= y 0)
         (< x (tile-grid-cols g))
         (< y (tile-grid-rows g))
         (tile-grid-ref g x y)))
  (tile-grid-fold (lambda (x y yx res)
                    (let ((bx (1- x)) (ax (1+ x))
                          (by (1- y)) (ay (1+ y)))
                      (f x y
                         (ref bx by) (ref x by) (ref ax by)
                         (ref bx  y) yx         (ref ax  y)
                         (ref bx ay) (ref x ay) (ref ax ay))
                      res))
                  init g))
