# Lisp Game Jam 2022

This repository contains my work-in-progress entry to the [Lisp Game Jam
2022][lgj22] using the [Guile Scheme][guile] programming language and
the [Chickadee][chickadee] game development toolkit.

# Inspirations

I'm a life-long fan of the traditional roguelike genre (as described by
the ["Berlin Interpretation"][berlin]), growing up with classics like
[Zangband][zangband] and [ADOM][adom].  More recently I've enjoyed
[Cataclysm: Dark Days Ahead][cdda], [Jupiter Hell][jupiterhell], and
[Sil-Q][sil].

Another major influence are the fourth and fifth generation of console
games, particularly the SNES classics *Chrono Trigger* and *Terranigma*,
the PSX masterpiece *Castlevania: Symphony of the Night*, and the
grand-daddy of real-time roguelike-likes: *Diablo*.

# Jam Goals

Having never participated in a game jam before, never written anything
which would qualify as "video game" rather than a "tech demo" before,
and never written anything game-related in a Lisp/Scheme before, I've
decided to write a roguelike game from scratch for this jam.

The entry should have the following features:

 - turn-based & grid-based gameplay
 - procedural dungeon generation
 - successively harder dungeon levels
 - a final "impossibly difficult" boss fight
 - VGA graphics & pixel art & sprite animations
 - gamepad friendly (4-directional movement, 2-button UI)

I'd also like to achieve the following:

 - overlay-based "dynamic" lighting system
 - inventory system with multiple item types
 - multiple weapons with animated abilities
 - multiple enemy types with varied tactical AI
 - ambient audio & sound effects

"Aim low", they said!

# Pre-Jam Preparation

The Jam rules say "You may develop your own general-purpose engine,
tools, and assets before the jam begins."  To that end, I've been
hunting for free pixel art assets, getting familiar with
[Libresprite][libresprite], and putting together tech demos with
[Chickadee][chickadee].

## Pixel-art

Looking for something suitably free and dungeon-crawl-ish, I've settled
on *0x72's 16x16 Dungeon Tileset II* as a base.  This tileset has a
decent amount of variety, a number of community additions, and an art
style simple enough to be tweaked by a programmer.

 - https://0x72.itch.io/dungeontileset-ii
 - https://nijikokun.itch.io/dungeontileset-ii-extended
 - https://nitie.itch.io/0x72-dungeon-animations
 - https://safwyl.itch.io/16x16-dungeon-autotile-remix
 - https://aekae13.itch.io/16x16-dungeon-walls-reconfig
 - https://flatwhatson.itch.io/16x16-dungeontileset-tweaked

## Autotiling

![Screenshot of autotiling demo](screenshots/2022-10-26-autodungeon-clear-color.png)

[Autotiling demo code](examples/autodungeon.scm)

Autotiling is an algorithm for rendering a game level by selecting the
appropriate sprite for a map tile using a bitmask of the neighbouring
wall & floor tiles.  This should allow me to render a traditional
roguelike ASCII grid as pixel-art tiles.

In practice the autotiling is a bit restrictive because not all tile
variations are implemented.  Ideally I should write some code to compose
existing tiles to handle the edge-cases.  Otherwise I'll need to code
some limitations into the dungeon generation so we don't generate
un-renderable levels.

# In-Jam Preparation

Predicably, I didn't get all my prep work completed before the jam
started, so the first weekend of the jam has been spent getting basic
engine functionality in place.

## Sprite Animations

![Screenshot of animation demo](screenshots/2022-10-30-animations-bigger.gif)

[Animation demo code](examples/animation.scm)

Sprite animation is just displaying the correct sprite at the correct
location and the correct time.  The first challenge is to load the
animation frames from the correct locations in the spritesheet.  Once
they're loaded, Chickadee's scripting engine makes it easy to define a
schedule for displaying them.

## UI & Input

![Screenshot of 9-patch borders](screenshots/2022-10-30-ui-borders-text.png)

[Interface demo code](examples/interface.scm)

The game UI needs to be able to draw boxes on the screen, and draw some
text and icons on top of those boxes.  Chickadee provides a very useful
utility for drawing boxes called a 9-patch.  A 9-patch uses a template
sprite, and scale it to any dimensions by tiling or stretching the
middle parts while leaving the corners intact.  With a little trial and
error it's possible to put together an adequate layered UI.

TODO: demo input handling

# License

All code is GNU GPLv3+.

All art is CC0.

[lgj22]: https://itch.io/jam/lisp-game-jam-2022
[guile]: https://www.gnu.org/software/guile/
[chickadee]: https://dthompson.us/projects/chickadee.html
[berlin]: http://www.roguebasin.com/index.php/Berlin_Interpretation
[zangband]: http://www.roguebasin.com/index.php/Zangband
[adom]: http://roguebasin.com/index.php/ADOM
[cdda]: https://cataclysmdda.org/
[jupiterhell]: https://jupiterhell.com/
[sil]: http://roguebasin.com/index.php/Sil
[libresprite]: https://libresprite.github.io/
