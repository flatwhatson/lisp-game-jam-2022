(use-modules (chickadee)
             (chickadee math vector)
             (chickadee graphics sprite)
             (chickadee graphics texture)
             (lgj22 config)
             (lgj22 data tile-grid)
             (srfi srfi-60))

(define (autotile-image)
  (data-file "tiles/dungeon-tileset-ii-autotile-round-dark.png"))

(define empty-tile #\#)

(define-syntax-rule (grid rows ...)
  (strings->tile-grid `(rows ...) empty-tile))

(define (autotile-template)
  (grid "############..########..####.#######"
        "#.##.......#............#..........#"
        "#.##.##.##.##.##....##.##..........#"
        "#.##.##.##.##.##....##.##..#..###..#"
        "#.##.......##..........##.....###..."
        "#.##.##.##.##..........##....####..#"
        "#.##.##.##.##..........##.......#..#"
        "#.##.......##..........#...........#"
        "#############.##....##.##.....#....#"
        "#############.##....##.##..........#"
        "#.##.......#............#..........#"
        "############..########..#######.####"))

(define (generate-dungeon)
  (grid "###########################"
        "#.......##.......##.......#"
        "#.......##.......##.......#"
        "#.......##.......##.......#"
        "#.......##.......##.......#"
        "####.########.########.####"
        "####.########.########.####"
        "####.########.########.####"
        "####.########.########.####"
        "####.########.########.####"
        "####.##................####"
        "####.##.#####.########.####"
        "####.##.#####.########.####"
        "####.##.#####.########.####"
        "####.##.#####.########.####"
        "####.##.#####.########.####"
        "#..........##....##.......#"
        "#..........##....##.......#"
        "#..........##....##.......#"
        "#..........##....##.......#"
        "###########################"))

(define (floor-tile? tile)
  (not (eqv? empty-tile tile)))

(define (make-mask . tiles)
  (list->integer (map floor-tile? tiles)))

(define (make-mask-lookup grid)
  ;; Calculate the bitmask for each 3x3 tile, and store a mapping from
  ;; the bitmask to the tile index.  This will be used as a lookup to
  ;; find the correct tile when rendering maps.
  (let ((lookup (make-hash-table)))
    (tile-grid-fold-3x3
     (lambda (x y nw n ne w elem e sw s se ix)
       (let ((mask (make-mask nw n ne w elem e sw s se)))
         (hashv-set! lookup mask ix)
         (1+ ix)))
     0 grid)
    lookup))

(define (autotile-batch-3x3-tiles grid lookup atlas batch)
  ;; Calculate the bitmask for each 3x3 tile, use the lookup to find the
  ;; correct tile in the atlas, and add that tile to the sprite batch.
  (tile-grid-fold-3x3
   (lambda (x y nw n ne w elem e sw s se _)
     (let* ((mask (make-mask nw n ne w elem e sw s se))
            (index (hashv-ref lookup mask)))
       (cond (index
              (let ((pos (vec2 (* (+ x 3) 16)
                               (* (+ y 3) 16)))
                    (tile (texture-atlas-ref atlas index)))
                (sprite-batch-add! batch pos #:texture-region tile)))
             (else
              (format #t "Unsupported tile at (~a,~a):\n" x y)
              (format #t " ~a ~a ~a\n" nw n ne)
              (format #t " ~a ~a ~a\n" w elem e)
              (format #t " ~a ~a ~a\n" sw s se)))))
   #t grid))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define texture (load-image (autotile-image)))
(define tileset (split-texture texture 48 48))
(define batch (make-sprite-batch texture))

(let ((lookup (make-mask-lookup (autotile-template)))
      (dungeon (generate-dungeon)))
  (sprite-batch-clear! batch)
  (autotile-batch-3x3-tiles dungeon lookup tileset batch))

(define (draw alpha)
  (draw-sprite-batch batch))
