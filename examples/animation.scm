(use-modules (chickadee)
             (chickadee math vector)
             (chickadee graphics sprite)
             (chickadee graphics texture)
             (chickadee scripting)
             (chickadee scripting script)
             (lgj22 config)
             (srfi srfi-9))

(define-record-type <animation>
  (%make-animation pos ix atlas)
  animation?
  (pos animation-pos set-animation-pos!)
  (ix animation-ix set-animation-ix!)
  (atlas animation-atlas))

(define texture
  ;; https://0x72.itch.io/dungeontileset-ii
  (load-image (data-file "sheets/0x72_DungeonTilesetII_v1.4.png")))

(define animation-specs
  ;; These animation specs were kindly provided by the artist, and
  ;; specify everything we need to load a particular animation.
  ;;
  ;; (KEY X Y FRAME-WIDTH FRAME-HEIGHT NUM-FRAMES)
  '((elf-gal-idle 128 4 16 28 4)
    (elf-gal-run 192 4 16 28 4)
    (elf-guy-idle 128 36 16 28 4)
    (elf-guy-run 192 36 16 28 4)
    (goblin-idle 368 32 16 16 4)
    (goblin-run 432 32 16 16 4)
    (masked-orc-idle 368 172 16 20 4)
    (masked-orc-run 432 172 16 20 4)
    (orc-warrior-idle 368 204 16 20 4)
    (orc-warrior-run 432 204 16 20 4)
    (orc-shaman-idle 368 236 16 20 4)
    (orc-shaman-run 432 236 16 20 4)))

(define (animation-spec->atlas tex x y w h n)
  ;; Build a texture atlas containing the frames described by the
  ;; animation spec.  Frames are organised in rows, so we add the width
  ;; to the X coordinate to get each successive frame.
  (let loop ((x x)
             ;; The specs above are based on (0,0) at top left, as in
             ;; libresprite, but Chickadee expects (0,0) at bottom left.
             (y (- (texture-height tex) y h))
             (n n)
             (rects '()))
    (if (zero? n)
        (list->texture-atlas tex (reverse rects))
        (loop (+ x w) y (1- n)
              (cons (list x y w h) rects)))))

(define (make-animation tex x y key)
  (let* ((spec (assq-ref animation-specs key))
         (atlas (apply animation-spec->atlas tex spec)))
    (%make-animation (vec2 x y) 0 atlas)))

(define idle-animations
  (list (make-animation texture 32 250 'elf-gal-idle)
        (make-animation texture 64 250 'elf-guy-idle)
        (make-animation texture 32 200 'goblin-idle)
        (make-animation texture 64 200 'masked-orc-idle)
        (make-animation texture 96 200 'orc-warrior-idle)
        (make-animation texture 128 200 'orc-shaman-idle)))

(define run-animations
  (list (make-animation texture 64 250 'elf-gal-run)
        (make-animation texture 128 250 'elf-guy-run)
        (make-animation texture 64 150 'goblin-run)
        (make-animation texture 128 150 'masked-orc-run)
        (make-animation texture 192 150 'orc-warrior-run)
        (make-animation texture 256 150 'orc-shaman-run)))

(define framerate 5)

(define batch (make-sprite-batch texture))

(for-each (lambda (anim)
            ;; Increment the frame counter at the specified framerate.
            ;; Loop back to the start after displaying the last frame.
            (every framerate
              (let* ((size (texture-atlas-size (animation-atlas anim)))
                     (next (modulo (1+ (animation-ix anim)) size)))
                (set-animation-ix! anim next))))
          run-animations)

(define (draw alpha)
  (sprite-batch-clear! batch)
  (for-each (lambda (anim)
              ;; Draw the current animation frame.  A sprite batch is
              ;; used to minimize the number of draw calls.
              (let* ((atlas (animation-atlas anim))
                     (index (animation-ix anim))
                     (frame (texture-atlas-ref atlas index))
                     (pos (animation-pos anim)))
                (sprite-batch-add! batch pos #:texture-region frame #:scale (vec2 4 4))))
            run-animations)
  (draw-sprite-batch batch))

(define (update dt)
  (update-agenda 1))
