(use-modules (chickadee)
             (chickadee math rect)
             (chickadee math vector)
             (chickadee graphics 9-patch)
             (chickadee graphics color)
             (chickadee graphics text)
             (chickadee graphics texture)
             (lgj22 config))

(define paper1 (load-image (data-file "ui/paper1.png")))
(define paper2 (load-image (data-file "ui/paper2.png")))

(define border1 (load-image (data-file "ui/border1.png")))
(define border2 (load-image (data-file "ui/border2.png")))
(define border3 (load-image (data-file "ui/border3.png")))

(define text-font (load-font (data-file "fonts/DungeonFont.ttf") 12))
(define text-color (rgb #x483b3a))

(define scale 4)

(define (draw-bordered-text border text x y w h)
  (draw-9-patch paper2 (rect (+ x (* scale 5))
                             (+ y (* scale 5))
                             (- w 10) (- h 10))
                #:margin 4 #:mode 'tile #:scale (vec2 scale scale))
  (draw-9-patch border (rect x y w h)
                #:margin 8 #:mode 'tile #:scale (vec2 scale scale))
  (draw-text text (vec2 (+ x (* scale 8))
                        (+ y (* scale 8)))
             #:font text-font #:color text-color #:scale (vec2 scale scale)))

(define (draw alpha)
  (draw-bordered-text border1 "Hello" 64 20 128 32)
  (draw-bordered-text border2 "Greetings" 64 180 128 32)
  (draw-bordered-text border3 "Salutations" 64 320 128 32))
